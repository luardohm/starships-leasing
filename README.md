This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
## It is a list of all Starwars Starships from the Milenial Falcon to the X Fighters.


## Features
++ Implemented with React router.

++ Tests with jest and enzyme.

++ add service to fetch resources from SWAPI.

++ add PDP for starships.

++ tracking library tracks user click on item and add to cart.

++ done with Atomic design structure, and CSS with BEM convention.

++ To compile the PWA run 'npm run build'

++ Basic responsive design (animations and more details pending)

## Pending features
-- Offline version .. If the serviceWorker is enable the calls to the api will not work, there for I didnt activate it yet. 

-- Homepage loads only the first page of the ships list. Implement pagination (pending)

-- Starship list page. Implement filters (pending)

-- Add search of ships (pending)


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

