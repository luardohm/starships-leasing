import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import './App.css';

import Home from './Components/Pages/Home';
import StarshipPage from './Components/Pages/StarshipPage';

import Header from './Components/Organisms/Header/Header';

const App = () => (
  <Router>
      <Header title="lease a starship" subtitle="The first Starwars starship leasing"/>
      <div className="content">
        <Route exact path="/" component={Home} />
        <Route exact path="/starship/:id" component={StarshipPage} />
      </div>
    </Router>
);

export default App;
