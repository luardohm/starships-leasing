import axios from 'axios'

class StarWarsApiService {
    constructor() {
        this.baseUrl = "https://swapi.co/api/";
        this.endpoints = {
            starships: "starships"
        };
    }

    getStarShipsList(page = null) {
        const requestUrl = page && this.baseUrl + this.endpoints.starships + '/?page=' + page
        || this.baseUrl + this.endpoints.starships;

        return axios.get(requestUrl)
            .then(({ data }) => data);
    }

    getStarShipDetails(id) {
        return axios.get(this.baseUrl + this.endpoints.starships + '/' + id + '/')
            .then(({ data }) => data);
    }
}

export default StarWarsApiService;