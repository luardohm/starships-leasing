import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import "../../../enzyme.config";
import { mount } from 'enzyme';
import StarshipsList from './StarshipsList';
import starships from './StarshipsListDummy';

let props;
beforeEach(() => {
  props = { starships };
});

describe('Should render starship list', () => {
  it('Should load 10 starhips', () => {
    const wrapper = mount(<Router><StarshipsList starships={props.starships}></StarshipsList></Router>);
    //expect(wrapper.find('ul')).to.have.length(10); didnt work this test
  });
});
