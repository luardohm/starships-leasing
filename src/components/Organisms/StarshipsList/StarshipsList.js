import React from 'react';
import ShipItem from '../../Molecules/ShipItem/ShipItem';

import './StarshipsList.scss';

const StarshipsList = ({ starships }) => {
    const StarShipsItems = starships.map(ship =>
        <li className="starships-list__item" key={ship.url} >
            <ShipItem
                url={ship.url}
                name={ship.name}
                model={ship.model}
                manufacturer={ship.manufacturer}>
            </ShipItem>
        </li>);

    return (
        <ul className="starships-list">
            {StarShipsItems}
        </ul>
    )
};

export default StarshipsList;