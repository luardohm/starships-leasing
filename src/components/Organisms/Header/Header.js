import React from 'react';
import { Link } from "react-router-dom";

import hamburger from '../../../svg/hamburger.svg';
import './Header.scss';

const Header = ({ title }) => (
    <div className="Header">
        <div className="Header__logo">
            <Link to="/">{title}</Link>
        </div>
        <div className="Header__menu">
            <img src={hamburger} alt="logo" />
        </div>
    </div>
)

export default Header;
