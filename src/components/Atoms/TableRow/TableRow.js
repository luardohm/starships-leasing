import React from 'react';

import './TableRow.scss';

const TableRow = ({label, value}) => (
    <div className="table-row">
        <strong>{label}:</strong> {value}
    </div>
)

export default TableRow;