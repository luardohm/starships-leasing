import React from 'react';
import './HeroBanner.scss';

const HeroBanner = ({ title, subtitle }) => (
    <div className="HeroBanner">
        <div className="HeroBanner__inner">
            <div className="HeroBanner__campaign">
                <h1>{title}</h1>
                <h2>{subtitle}</h2>
            </div>
        </div>
    </div>
)

export default HeroBanner;
