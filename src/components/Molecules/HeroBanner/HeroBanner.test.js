import React from 'react';
import "../../../enzyme.config";
import { shallow } from 'enzyme';
import HeroBanner from './HeroBanner';

describe('Should display a given title and subtitle for the banner', () => {
  it('Contains title', () => {
    const wrapper = shallow(<HeroBanner title="Hello" subtitle="Discover now!" />);
    const text = wrapper.find('h1').text();
    expect(text).toEqual('Hello');
  });

  it('Contains subtitle', () => {
    const wrapper = shallow(<HeroBanner title="Hello" subtitle="Discover now!" />);
    const text = wrapper.find('h2').text();
    expect(text).toEqual('Discover now!');
  });
});
