import { Link } from "react-router-dom";
import React from 'react';

import Button from '../../Atoms/Button/Button';
import Tracking from '../../../lib/Tracking';

import './ShipItem.scss';

class ShipItem extends React.Component {
    constructor(props) {
        super(props);
        this.tracking = new Tracking();
        this.getIdFromUrl = this.getIdFromUrl.bind(this);
        this.trackClick = this.trackClick.bind(this);
    }

    trackClick() {
        const { model, name, manufacturer, url } = this.props;
        this.tracking.push('clickOnItem', {
            model,
            name,
            manufacturer,
            url
        });
    }

    getIdFromUrl(url) {
        const cleanUrl = url.replace(/\/$/, "");
        return cleanUrl.substr(cleanUrl.lastIndexOf('/') + 1);
    }

    render() {
        const { model, name, manufacturer, url } = this.props;
        return (
            <Link className="ship-item" to={"/starship/" + this.getIdFromUrl(url)} onClick={this.trackClick}>
                <div className="ship-item__details">
                    <div className="ship-item__model">
                        <strong>{name}/{model}</strong>
                    </div>
                    <div className="ship-item__manufacturer">
                        {manufacturer}
                    </div>
                    <div className="ship-item__button-container">
                        <Button title="View plans" />
                    </div>
                </div>
            </Link>
        );
    }
}



export default ShipItem;
