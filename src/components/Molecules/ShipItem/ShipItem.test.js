import React from 'react';
import "../../../enzyme.config";
import { shallow } from 'enzyme';
import ShipItem from './ShipItem';

let props;
beforeEach(() => {
  props = {
    ship: {
      "name": "Calamari Cruiser",
      "model": "MC80 Liberty type Star Cruiser",
      "manufacturer": "Mon Calamari shipyards",
      "cost_in_credits": "104000000",
      "length": "1200",
      "max_atmosphering_speed": "n/a",
      "crew": "5400",
      "passengers": "1200",
      "cargo_capacity": "unknown",
      "consumables": "2 years",
      "hyperdrive_rating": "1.0",
      "MGLT": "60",
      "starship_class": "Star Cruiser",
      "pilots": [],
      "films": [
        "https://swapi.co/api/films/3/"
      ],
      "created": "2014-12-18T10:54:57.804000Z",
      "edited": "2014-12-22T17:35:44.957852Z",
      "url": "https://swapi.co/api/starships/27/"
    }
  };
});

describe('Should display the starship name and model', () => {
  it('Contains name and model of ship', () => {
    const { ship } = props;
    const wrapper = shallow(<ShipItem
      key={ship.url}
      url={ship.url}
      name={ship.name}
      model={ship.model}
      manufacturer={ship.manufacturer}></ShipItem>);

    const text = wrapper.find('.ship-item__model').text();
    expect(text).toEqual('Calamari Cruiser/MC80 Liberty type Star Cruiser');
  });

  it('Should id from url to be 27', () => {
    const { ship } = props;
    const wrapper = shallow(<ShipItem
      key={ship.url}
      url={ship.url}
      name={ship.name}
      model={ship.model}
      manufacturer={ship.manufacturer}></ShipItem>);
    expect(wrapper.instance().getIdFromUrl(ship.url)).toEqual('27');
  });
});
