import React from 'react';
import ReactDOM from 'react-dom';

import AddToCart from './AddToCart';

it('renders a add to cart component', () => {
    const div = document.createElement('div');
    ReactDOM.render(<AddToCart />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('should contain a price', () => {
    const div = document.createElement('div');
    ReactDOM.render(<AddToCart price="299"/>, div);
    ReactDOM.unmountComponentAtNode(div);

  });