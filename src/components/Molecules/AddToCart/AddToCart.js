import React from 'react';
import Button from '../../atoms/button/Button';
import Price from '../../atoms/price/Price';


class AddToCart extends React.Component {
    constructor(props) {
        super(props);
        this.sendRequest = this.sendRequest.bind(this);
    }

    sendRequest(id) {
        return true;
    }

    render() {
        const { id } = this.props;
        return (
            <div className="add-to-cart">
                <Price price="299" conditions="month"></Price>
                <Button title="Add to cart" onclick={this.sendRequest(id)}></Button>
            </div>
        );
    }
}



export default AddToCart;
