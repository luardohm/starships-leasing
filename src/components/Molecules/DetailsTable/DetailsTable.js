import React from 'react';
import TableRow from '../../Atoms/TableRow/TableRow';

const DetailsTable = ({details: 
    {manufacturer, length, passengers, cargo_capacity, consumables, starship_class, crew}}) => (
    <div className="details-table">
        <TableRow label="Manufacturer" value={manufacturer} />
        <TableRow label="Length" value={length} />
        <TableRow label="Number of Passengers" value={passengers} />
        <TableRow label="Cargo Capacity" value={cargo_capacity} />
        <TableRow label="Consumables" value={consumables} />
        <TableRow label="Class" value={starship_class} />
        <TableRow label="Crew" value={crew} />
    </div>
)

export default DetailsTable;