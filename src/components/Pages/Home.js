import React, { Component } from 'react';

import StarwarsApiService from '../../Services/StarWarsApiService';
import HeroBanner from '../../Components/Molecules/HeroBanner/HeroBanner';
import StarshipsList from '../../Components/Organisms/StarshipsList/StarshipsList';

class Home extends Component {

  constructor(props) {
    super(props);
    this.StarwarsApiService = new StarwarsApiService();

    this.state = {
      starships: [],
      count: 0
    };
  }

  componentWillMount() {
    this.getStarShips();
  }

  getStarShips(page = null) {
    this.StarwarsApiService.getStarShipsList(page)
    .then(({ results: starships, count }) => {
      this.setState({
        starships,
        count
      });
    })
    .catch(console.error);
  }

  render() {
    const { starships, count } = this.state;
    let startShipsComponent = null;

    if (starships.length > 0) {
      startShipsComponent = <StarshipsList starships={starships} />;
    } else {
      startShipsComponent = <div>Loading ...</div>
    }

    return (
      <div>
        <HeroBanner title="Get your starship!" subtitle="The first Starship leasing marketplace" />
        {startShipsComponent}
        <div className="count">{count}</div>
      </div>
    );
  }
}

export default Home;