import { Link } from "react-router-dom";
import React, { Component } from 'react';
import Tracking from '../../lib/Tracking';

import Button from '../Atoms/Button/Button';
import DetailsTable from '../Molecules/DetailsTable/DetailsTable';
import HeroBanner from '../../Components/Molecules/HeroBanner/HeroBanner';
import StarwarsApiService from '../../Services/StarWarsApiService';

import './StarshipPage.scss';

class StarshipPage extends Component {

  constructor(props) {
    super(props);
    this.tracking = new Tracking();
    this.StarwarsApiService = new StarwarsApiService();
    this.trackAddToCart = this.trackAddToCart.bind(this);

    this.state = {
      starship: {}
    };
  }

  componentWillMount() {
    this.getStartshipDetails();
  }

  updateStarship(starship) {
    this.setState({
      starship
    });
  }

  getStartshipDetails() {
    const { id } = this.props.match.params;
    this.StarwarsApiService.getStarShipDetails(id)
      .then((response) => {
        this.updateStarship(response);
      })
      .catch(console.error);
  }

  trackAddToCart() {
    const {
      starship:
      { name, model, manufacturer }
    } = this.state;

    this.tracking.push('addToCart', {
      model,
      name,
      manufacturer
    });
  }

  render() {

    const {
      starship,
      starship:
      { name, model, cost_in_credits }
    } = this.state;

    return (
      <div>
        <HeroBanner modifierClass="lean" title={name} subtitle={model} />
        <div className="details-container">
          <div className="details-container__cta">
            <div className="details-container__price"><strong>Price:</strong> {cost_in_credits}</div>
            <Button title="Lease now!" onClick={this.trackAddToCart} />
            <small><Link to="/">Go Back</Link></small>



          </div>

          <div className="details-container__specs">
            <DetailsTable details={starship} />
          </div>
        </div>
      </div>
    )
  }
}

export default StarshipPage;