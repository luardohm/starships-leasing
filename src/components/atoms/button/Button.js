import React from 'react';
import './Button.scss';

const Button = ({title}) => 
<div className="button">{title}</div>;

export default Button;
