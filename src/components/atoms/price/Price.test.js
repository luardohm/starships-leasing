import React from 'react';
import ReactDOM from 'react-dom';

import Price from './Price';

it('renders a price element', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Price />, div);
    ReactDOM.unmountComponentAtNode(div);
  });