import React from 'react';

const Price = ({price, discount, discountedPrice, conditions}) => 
<div className="price">€{price} p/{conditions}</div>;

export default Price;
