class Tracking  {

    constructor() {
        this.dataLayer = {};

        if(!localStorage.getItem('dataLayer')) {
            this.hash = this.createHash();
            this.dataLayer[this.hash] = {};
            localStorage.setItem('session', this.hash);
            localStorage.setItem('dataLayer', JSON.stringify(this.dataLayer));
        }

    }

    createHash() {
        return  localStorage.getItem('session') ||
        Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    }

    push(eventName, value) {
        this.dataLayer = JSON.parse(localStorage.getItem('dataLayer'));
     
        if(this.dataLayer[this.hash].hasOwnProperty(eventName) ) {
            this.dataLayer[this.hash][eventName].push(value);
        } else {
            this.dataLayer[this.hash][eventName] = [value]
        }

        console.log(this.dataLayer);
        this.toLocalStorage();
    }

    
    toLocalStorage() {
        localStorage.setItem('dataLayer', JSON.stringify(this.dataLayer));
    }

    resetDataLayer() {
        localStorage.removeItem('dataLayer');
    }

}

export default Tracking;